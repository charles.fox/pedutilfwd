
#Project created Jan 30th, 2020

import numpy as np
import math
import pdb
import random
import matplotlib.pyplot as plt
from numpy.testing import assert_almost_equal
import operator
import scipy.stats
from scipy.optimize import curve_fit


Y_MAX=10

#ped is X, car is Y


#the car goes at a constant speed, it does not interact
def moveCar(y, carspeed):
    y_new = y+carspeed
    return y_new

def movePed(x,y, pedspeed, carspeed, U_ped_prox_fn):
    
    #assert_almost_equal(x, 1.0)    # this constraint the simulation to be only one time step
    assert_almost_equal(pedspeed, 1.0)
    U_timedelay = 1.           #value of one tick (second) unit of time in dollars    # changed to +1., cf. comment below for  U_ped_prox in the main function
    b_carHasPassed = (y<=0.)     # only when car passes over 0 
    b_pedHaspassed = (x<=0.)   # check whether the ped has already crossed
    if b_carHasPassed or b_pedHaspassed:
        x_new = x - pedspeed   #cross
        U_prox_given_walk = 0.

    if not b_carHasPassed and not b_pedHaspassed :      
        veh_time_to_inters = (y-1)/abs(carspeed)     # use y-1 corrrespond to time for the car to reach the middle of the road
        ped_time = x/pedspeed    #time it would take ped to cross

        assert_almost_equal(ped_time, 1.0)
        
        U_dontwalk = veh_time_to_inters * U_timedelay   #value of waiting for the vehicle to cross
        print("Udont_walk: " + str(U_dontwalk))

        #TODO compute U_prox.  In theory, U_prox=\int dt U_ped_prox(x,y)  . We approximate with the single longest arrow.
        #(we assume that the interaction lasts for a infinitely small time)
        #eg. car could be 2km away now.  but it will be closer in the future at time t2. 
        U_prox_given_walk = U_ped_prox_fn(y)    #TODO think about the details and assumptions/approximations here
        print("U_prox fn: " + str(U_ped_prox_fn(y)))
       
        U_walk = U_prox_given_walk
  
        if U_walk < U_dontwalk:    #make our action selection decision      # was originally U_walk>U_dontwalk but due to change of sign in U_ped_prox and U_time, this should change too.  
            x_new = x - pedspeed
        else:
            x_new = x

    return x_new  


def simulate(U_ped_prox, y_init, carspeed):
    T = 2    # only runs for one time: walk or stop
    x_init = 1.0  #start meters from collision point for pedestrian
    pedspeed = 1.0 #ped speed 1 meter per second

    xs=[x_init]
    ys=[y_init]

    x=x_init
    y=y_init    
    for t in range(0, T):
        x = movePed(x,y, pedspeed, carspeed, U_ped_prox)
        y = moveCar(y,carspeed)

        xs.append(x)
        ys.append(y)
    return (xs,ys)


def simulateMany(U_ped_prox, carspeed):
    epochs = 1000   #how many simulations to run for one set of parameters

    alldata = []
    for epoch in range(0, epochs):
        y_init = round(random.uniform(0.01,Y_MAX), 2)   #meters distance from collision point for car start location, random selection
        (xs, ys) = simulate(U_ped_prox, y_init, carspeed) 
        
        simdata = (y_init, carspeed, xs, ys) 
        alldata.append(simdata)
    
    for simdata in alldata:
        print(simdata)
    
    return (alldata)
    
    
### This function converts interaction into a game and returns a list of games as (y, x, a_y, a_x)
def createGames(simData, carSpeed):
    allgames = []
    for sim in simData:
        games = []
        x_prev = 2.
        for i in range(0, len(sim[2])):
            y = sim[3][i]
            x = sim[2][i]
            if(y>-2. and x>-2.):
                a_y = carSpeed
                a_x = x_prev - x
                turn = (y, x, a_y, a_x)
                x_prev = x
                games.append(turn)
        allgames.append(games)
    #print("Games:")
    #print(allgames)
    return allgames		   

       
### This function works similarly to range() but working also for decimals
def frange(start, stop=None, step=None):
    # if stop and step argument is None set start=0.0 and step = 1.0
    start = float(start)
    if stop == None:
        stop = start + 0.0
        start = 0.0
    if step == None:
        step = 1.0

    count = 0
    while True:
        temp = float(start + count * step)
        if step > 0 and temp >= stop:
            break
        elif step < 0 and temp <= stop:
            break
        yield temp
        count += 1

### plot Ground Hyperbolic Function
def plotGroundTruthProxemicFunction():
    y = [x for x in frange(0.01, Y_MAX, 0.01)]
    hyperbola = [-1./x for x in y]
    plt.plot(y, hyperbola, color='g', label='Ground Truth')


### This functions computes P(datum|model, params)
def getP_data_given_any_Model(U_prox_fn, datum, carSpeed):
    print("Entering simulation")
    ycur, xac = datum
    (xSim, ySim) = simulate(U_prox_fn, ycur, carSpeed)   # simulate given new utility function
    print("xSim: " + str(xSim))
    noise = 0.1
    print("Exiting simulation")
    #what is the prob of datum=CROSS ?
    if xSim[1] == 0.:
        pCross = 1. - noise
        pStop = noise
    #what is the prob of datum=DONTCROSS ?
    else: 
        pStop = 1. - noise
        pCross = noise
        
    #did the pedestrian cross?
    if xac == 0.:
        prob = pCross
    else:
        prob = pStop
    print("Prob: " + str(prob))
    
    if (prob<0.) or (prob>1.0):
        print("this should never happen!")

    #return the probability of what they did
    return prob 


### This functions computes P(data|model, params)
def getLogLik_data_given_any_Model(games, U_prox_fn, carSpeed):

    loglik_total = 0
    for game in games:
        (ycur, xcur, a_y, a_x) = game[0]
        (yac, xac, a_y, a_x) = game[1]
        datum = (ycur, xac)
        print("Datum: " + str(datum))
        probDatum = getP_data_given_any_Model(U_prox_fn, datum, carSpeed)
        loglik = np.log(probDatum)
        print("Loglik od datum: " + str(loglik))
        loglik_total += loglik  
    return loglik_total

 
### This function search for optimal parameters
def getOptimalParameters_given_HyperbolicModel(data, carSpeed):

    games = createGames(data, carSpeed)
    ycar = np.array([x[0][0] for x in games])
    xped = np.array([x[1][1] for x in games])
    
    ### Hyperbolic fitting
    popt, pcov = curve_fit(HyperbolicFunc, ycar, xped, bounds=[1.,5])  ## added constraint due scipy's tendency to assign very small values to the parameters
        
    params = popt
    Hyperbolic_fn = (lambda ydat: params/ydat)  #creates a lambda function for Hyperbolic model
    loglik = getLogLik_data_given_any_Model(games, Hyperbolic_fn, carSpeed)
    bic = np.log(len(ycar))*len(params) - 2*loglik
    
    print("Scipy Hyperbolic Best params: " + str(popt))
    plt.plot(ycar, HyperbolicFunc(ycar, *popt), 'b*', label='Hyperbolic fit: a=%5.3f' % tuple(popt) + ' --- loglik =%5.6f' %(loglik) + ' --- BIC=%5.3f' %(bic)) 

    return (params, loglik, bic)
   
              
              
### This function search for optimal parameters in Gaussian Model
def getOptimalParameters_given_GaussianModel(data, carSpeed):

    games = createGames(data, carSpeed)
    ycar = np.array([x[0][0] for x in games])
    xped = np.array([x[1][1] for x in games])
    
    ### Gaussian fitting
    popt, pcov = curve_fit(GaussianFunc, ycar, xped, bounds=[0.01,Y_MAX])   ## added constraint due scipy's tendency to assign negative values to std 
    
    params = popt
    a, b = params
    var = b**2
    denom = (2*math.pi*var)**.5
    Gaussian_fn = (lambda x: math.exp(-(x-a)**2/(2*var))/denom)  #creates a lambda function
    loglik = getLogLik_data_given_any_Model(games, Gaussian_fn, carSpeed)
    bic = np.log(len(ycar))*len(popt) - 2*loglik
    
    print("Scipy Gaussian Best params: " + str(popt))
    plt.plot(ycar, GaussianFunc(ycar, *popt), 'r+', label='Gaussian fit: a=%5.3f, b=%5.3f' % tuple(popt) + ' --- loglik =%5.6f' %(loglik) + ' --- BIC=%5.3f' %(bic))

    return (params, loglik, bic)              

### This function search for optimal parameters in Polynomial 7 degree Model
def getOptimalParameters_given_Poly7degModel(data, carSpeed):

    games = createGames(data, carSpeed)
    ycar = np.array([x[0][0] for x in games])
    xped = np.array([x[1][1] for x in games])
    
    ### Poly 7 deg fitting
    param_bounds=([-np.inf,-np.inf,-np.inf, -np.inf, -np.inf, -np.inf, -np.inf,-np.inf],[-0.001,np.inf,np.inf, np.inf, np.inf, np.inf,np.inf, np.inf])
    popt, pcov = curve_fit(Poly7degFunc, ycar, xped, bounds=param_bounds)
    
    params = popt
    a, b, c, d, e, f, g, h = params
    Poly7deg_fn = (lambda x: a*x**7 + b*x**6 + c*x**5 + d*x**4 + e*x**3 + f*x**2 + g*x + h)  #creates a lambda function
    loglik = getLogLik_data_given_any_Model(games, Poly7deg_fn, carSpeed)
    bic = np.log(len(ycar))*len(params) - 2*loglik
    
    print("Scipy Poly 10deg Best params: " + str(popt))
    plt.plot(ycar, Poly7degFunc(ycar, *popt), 'm+', label='Polynomial deg 7 fit: a=%5.3f, b=%5.3f, c=%5.3f, d=%5.3f, e=%5.3f, f=%5.3f, g=%5.3f, h=%5.3f' % tuple(popt) + ' --- loglik =%5.6f' %(loglik) + ' --- BIC=%5.3f' %(bic))

    return (params, loglik, bic)


### This function search for optimal parameters in Polynomial 4 degree Model
def getOptimalParameters_given_Poly4degModel(data, carSpeed):

    games = createGames(data, carSpeed)
    ycar = np.array([x[0][0] for x in games])
    xped = np.array([x[1][1] for x in games])
    
    ### Poly 4 deg fitting
    param_bounds=([-np.inf,-np.inf,-np.inf, -np.inf, -np.inf],[-0.001,np.inf,np.inf, np.inf, np.inf])
    popt, pcov = curve_fit(Poly4degFunc, ycar, xped, bounds=param_bounds)
    
    params = popt
    a, b, c, d, e = params
    Poly4deg_fn = (lambda x: a*x**4 + b*x**3 + c*x**2 + d*x + e)  #creates a lambda function
    loglik = getLogLik_data_given_any_Model(games, Poly4deg_fn, carSpeed)
    bic = np.log(len(ycar))*len(params) - 2*loglik
    
    print("Scipy Poly 4deg Best params: " + str(popt))
    plt.plot(ycar, Poly4degFunc(ycar, *popt), 'c*', label='Polynomial deg 4 fit: a=%5.3f, b=%5.3f, c=%5.3f, d=%5.3f, e=%5.3f' % tuple(popt) + ' --- loglik =%5.6f' %(loglik) + ' --- BIC=%5.3f' %(bic))

    return (params, loglik, bic)
    
 
### This function search for optimal parameters in Cubic Model
def getOptimalParameters_given_CubicModel(data, carSpeed):

    games = createGames(data, carSpeed)
    ycar = np.array([x[0][0] for x in games])
    xped = np.array([x[1][1] for x in games])
    
    ### Cubic fitting
    param_bounds=([-np.inf,-np.inf,-np.inf, -np.inf],[-0.001,np.inf,np.inf, np.inf])
    popt, pcov = curve_fit(CubicFunc, ycar, xped, bounds=param_bounds)
    
    params = popt
    a, b, c, d = params
    Cubic_fn = (lambda x: a*x**3 + b*x**2 + c*x + d)  #creates a lambda function
    loglik = getLogLik_data_given_any_Model(games, Cubic_fn, carSpeed)
    bic = np.log(len(ycar))*len(params) - 2*loglik
    
    print("Scipy Cubic Best params: " + str(popt))
    plt.plot(ycar, CubicFunc(ycar, *popt), '+', color = 'darkorange', label='Cubic fit: a=%5.3f, b=%5.3f, c=%5.3f, d=%5.3f' % tuple(popt) + ' --- loglik =%5.6f' %(loglik) + ' --- BIC=%5.3f' %(bic))

    return (params, loglik, bic)
   
   
   
### This function search for optimal parameters in Parabolic Model
def getOptimalParameters_given_ParabolicModel(data, carSpeed):

    games = createGames(data, carSpeed)
    ycar = np.array([x[0][0] for x in games])
    xped = np.array([x[1][1] for x in games])
    
    ### Parabolic fitting
    param_bounds=([-np.inf,-np.inf,-np.inf],[-0.001,np.inf,np.inf])
    popt, pcov = curve_fit(ParabolicFunc, ycar, xped, bounds=param_bounds)
    
    params = popt
    a, b, c = params
    Parabolic_fn = (lambda x: a*x**2 + b*x + c)  #creates a lambda function
    loglik = getLogLik_data_given_any_Model(games, Parabolic_fn, carSpeed)
    bic = np.log(len(ycar))*len(params) - 2*loglik
    
    print("Scipy Parabolic Best params: " + str(popt))
    plt.plot(ycar, ParabolicFunc(ycar, *popt), '*', color='yellow', label='Parabolic fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt) + ' --- loglik =%5.6f' %(loglik) + ' --- BIC=%5.3f' %(bic))

    return (params, loglik, bic)

    
    
### This function search for optimal parameters in Parabolic Model
def getOptimalParameters_given_LinearModel(data, carSpeed):

    games = createGames(data, carSpeed)
    ycar = np.array([x[0][0] for x in games])
    xped = np.array([x[1][1] for x in games])
    
    ### Linear fitting
    param_bounds=([-np.inf,-np.inf],[-0.001,np.inf])
    popt, pcov = curve_fit(LinearFunc, ycar, xped, bounds=param_bounds)
    
    params = popt
    a, b = params
    Linear_fn = (lambda x: a*x + b)  #creates a lambda function
    loglik = getLogLik_data_given_any_Model(games, Linear_fn, carSpeed)
    bic = np.log(len(ycar))*len(params) - 2*loglik
    
    print("Scipy Linear Best params: " + str(popt))
    plt.plot(ycar, LinearFunc(ycar, *popt), 'k+', label='Linear fit: a=%5.3f, b=%5.3f' % tuple(popt) + ' --- loglik =%5.6f' %(loglik) + ' --- BIC=%5.3f' %(bic))

    return (params, loglik, bic)
    
                
### Hyperbolic Function 
def HyperbolicFunc(x, a):
    num = a/x
    return num
    
### Gaussian Function
def GaussianFunc(x, a, b):
    var = b**2
    denom = (2*math.pi*var)**.5
    num = np.exp(-(x-a)**2/(2*var))
    return num/denom
 
### Polynomial deg 7 
def Poly7degFunc(x, a, b, c, d, e, f, g, h):
    num = a*x**7 + b*x**6 + c*x**5 + d*x**4 + e*x**3 + f*x**2 + g*x + h
    return num
 
### Polynomial deg 4
def Poly4degFunc(x, a, b, c, d, e):
    num = a*x**4 + b*x**3 + c*x**2 + d*x + e
    return num

### Polynomial deg 3
def CubicFunc(x, a, b, c, d):
    num = a*x**3 + b*x**2 + c*x + d
    return num
    
### Polynomial deg 2
def ParabolicFunc(x, a, b, c):
    num = a*x**2 + b*x + c
    return num

### Linear Function     
def LinearFunc(x, a, b):
    num = a*x + b
    return num


def fitAllModels(dataGen, carSpeed):

    ### Hyperbolic Utility Model    
    (params, loglik, bic) = getOptimalParameters_given_HyperbolicModel(dataGen, carSpeed)
    print("Hyperbolic -- " + "Loglik: " + str(loglik) + " BIC: " + str(bic))

    ### Gaussian Utility Model    
    (params, loglik, bic) = getOptimalParameters_given_GaussianModel(dataGen, carSpeed)
    print("Gaussian -- " + "Loglik: " + str(loglik) + " BIC: " + str(bic))
    
    
    ### Polynomial 7 deg Utility Model    
    (params, loglik, bic) = getOptimalParameters_given_Poly7degModel(dataGen, carSpeed)
    print("Polynomial 7 deg -- " + "Loglik: " + str(loglik) + " BIC: " + str(bic))
    
    ### Polynomial 4 deg Utility Model    
    (params, loglik, bic) = getOptimalParameters_given_Poly4degModel(dataGen, carSpeed)
    print("Polynomial 4 deg -- " + "Loglik: " + str(loglik) + " BIC: " + str(bic))
    
    ### Cubic Utility Model    
    (params, loglik, bic) = getOptimalParameters_given_CubicModel(dataGen, carSpeed)
    print("Cubic -- " + "Loglik: " + str(loglik) + " BIC: " + str(bic))
    
    ### Parabolic Utility Model    
    (params, loglik, bic) = getOptimalParameters_given_ParabolicModel(dataGen, carSpeed)
    print("Parabolic -- " + "Loglik: " + str(loglik) + " BIC: " + str(bic))
    
    ### Linear Utility Model    
    (params, loglik, bic) = getOptimalParameters_given_LinearModel(dataGen, carSpeed)
    print("Linear -- " + "Loglik: " + str(loglik) + " BIC: " + str(bic))


if __name__=="__main__":
    ### parameters
    carSpeed = -2  #car speed in meters per second 
    U_PED_PROX_FN = (lambda dist: 1./dist)      # change this to +1./x because scipy assumes the functions are positive, whereas we were originally generating data with a negative utility, for the same reason, I changed U_time = +1.0
    
    ### data generation
    dataGen = simulateMany(U_PED_PROX_FN, carSpeed)
    
    ### Plot ground truth utility function
    #plotGroundTruthProxemicFunction()
    
    ### Fit all the models
    fitAllModels(dataGen, carSpeed)
    
   
    plt.grid(True)
    plt.xlabel('Y')
    plt.ylabel('U_prox')
    plt.xlim(0, Y_MAX)
    plt.ylim(0, 10)
    plt.title("Proxemic Utility Graph")
    plt.legend()
    plt.show()









