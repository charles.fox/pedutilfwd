
#Project created Jan 30th, 2020

import numpy as np
import pdb
import random
import matplotlib.pyplot as plt

Y_MAX=20

#ped is X, car is Y


#the car goes at a constant speed, it does not interact
def moveCar(y, carspeed):
    y_new = y+carspeed
    return y_new

def movePed(x,y, pedspeed, carspeed, U_ped_crash, U_ped_prox_fn):
    #assert(x>0.99 and x<1.01)
    #assert(pedspeed>0.99 and pedspeed<1.01)
    U_timedelay = -1.           #value of one tick (second) unit of time in dollars
    b_carHasPassed = (y<=0.)
    if b_carHasPassed:
        x_new = x - pedspeed   #cross

    if not b_carHasPassed:      
        veh_time_to_inters = y/abs(carspeed)
        ped_time = x/pedspeed    #time it would take ped to cross

        #assert(ped_time>0.99 and ped_time<1.01)

        U_dontwalk = veh_time_to_inters * U_timedelay   #value of waiting for the vehicle to cross 
        
        if veh_time_to_inters<=ped_time:
            p_crash_given_walk = 1.
        else:
            p_crash_given_walk = 0.

        #TODO compute U_prox.  In theory, U_prox=\int dt U_ped_prox(x,y)  . We approximate with the single longest arrow.
        #(we assume that the interaction lasts for a infinitely small time)
        #eg. car could be 2km away now.  but it will be closer in the future at time t2. 
        #what is the single WORST proxemic utility we will experience if we cross?
        U_prox_given_walk = U_ped_prox_fn(y)    #TODO think about the details and assumptions/approximations here
       
        U_walk = p_crash_given_walk*U_ped_crash + U_prox_given_walk
    
        if U_walk>U_dontwalk:    #make our action selection decision
            x_new = x - pedspeed
        else:
            x_new = x

    return x_new  


def simulate(U_ped_crash, U_ped_prox, y_init, carspeed):
    T = 100
    x_init = 1.0  #start meters from collision point for pedestrian
    pedspeed = 1.0 #ped speed 1 meter per second


    xs=[x_init]
    ys=[y_init]

    x=x_init
    y=y_init    
    for t in range(0, T):
        y = moveCar(y,carspeed)
        x = movePed(x,y, pedspeed, carspeed, U_ped_crash, U_ped_prox)

        xs.append(x)
        ys.append(y)
    return (xs,ys)

def simulateMany():
    U_ped_crash = -100.
    U_ped_prox = (lambda dist: -1./dist)

    epochs = 1000   #how many simulations to run for one set of parameters

    carspeed = -2.  #car speed in meters per second

    alldata = []
    for epoch in range(0, epochs):
        y_init = random.randint(1, Y_MAX)   #meters distance from collision point for car start location, random selection
        #print(y_init)
        (xs, ys) = simulate(U_ped_crash, U_ped_prox, y_init, carspeed) 
        
        simdata = (y_init, carspeed, xs, ys) 
        alldata.append(simdata)
    
    for simdata in alldata:
        print(simdata)
    
    return alldata
    
    
#fitting a parametric model to best explain the utility function
#eg if we assume an exponential model with parameters a and b then we will find a_hat and b_hat with the best fit
#a histogram model would have many free parameters and be able to fit any shape
#you could be frequentist here to estimate the model
#or be bayesian and use maxent priors etc
#a standard method is to iterate over many vector parameter values and ask which would generate the best fit -- brute force method
#or you could be more clever and use actual inference algorithms
#
# a good way to test your setup is to hypothresis the actual ground truth utility function (cheating) and test it
# it should give a better fit than any other function that you guess
# if you do this first, it will prove that your code is working!
#
#after that, you can search different parameter values without cheating.
#
#
#eg write a function to compute the data likelihood under hypothesised parameters
def getfitAssumingSomeModel(data, model_parameters):
    #TODO
    return P(data|model,parameters)  #TODO
    #TODO you could write versions of this function for many types of model, eg. histogram, hyperbola, exponential etc.


def findBestParameters(data):
    for parameter_values in lots_of_guesses:
        lik = getfit(data, model_parameters)

        #OPTIONAL:
        #if you want to compaere many differnet FORMS of model as well as parameters within a model, by estimating P(M|data), regularisation
        #lik = lik + BIC(nparameters) #where BIC= bayesian information criterion for n parmaeters, this is to compensate for overfitting many paraters.

    #if lik is better than all the others:
        return lik


def infer_utility(simdata):
    pedwalk = []
    pedstop = []
    #prob = 1/20
    
    # histogram data
    freq = np.histogram([x[0] for x in simdata],bins=Y_MAX)
    print(freq)
    print("Tab 0: " + str(freq[0][0]))
    plt.hist([x[0] for x in simdata],bins=Y_MAX)
    plt.show()
    
    # separate sim data into walk and stop
    for sim in simdata:
        if(sim[2][1] ==0):
            pedwalk.append(sim)
        else:
            pedstop.append(sim)
    print("Pedwalk: " + str(len(pedwalk)) + " Pedstop: " + str(len(pedstop)) + "\n\n\n")
    
    u = []
    x = 1.
    for i in range(0,Y_MAX):
        p_i = (freq[0][i]+0.0)/len(simdata)
        #print("len simdata: " + str(len(simdata)))
        #print("freq i: " +str(freq[0][i]))
        #print("p: " + str(p_i))
        u.append((i - x, -np.log((i-x)*p_i)))    # cf. Bernardo & Smith chapter 2.7 !
    return u
    
def plot_u(u):
	
	plt.scatter([x[0] for x in u], [x[1] for x in u], linewidth=0.5, color='m')
	plt.grid(True)
	plt.xlabel('Distance [m]')
	plt.ylabel('Utility')
	plt.title("utility graph")
	plt.show()	          
	

if __name__=="__main__":
    simdata = simulateMany()
    print("\n\n\n\n\n")
    utility = infer_utility(simdata)
    plot_u(utility)
