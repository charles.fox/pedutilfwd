
#Project created Jan 30th, 2020

import numpy as np
import math
import pdb
import random
import matplotlib.pyplot as plt
from numpy.testing import assert_almost_equal
import operator
import scipy.stats


Y_MAX=5

#ped is X, car is Y


#the car goes at a constant speed, it does not interact
def moveCar(y, carspeed):
    y_new = y+carspeed
    return y_new

def movePed(x,y, pedspeed, carspeed, U_ped_crash, U_ped_prox_fn):
    #assert_almost_equal(x, 1.0)    # this constraint the simulation to be only one time step
    assert_almost_equal(pedspeed, 1.0)
    U_timedelay = -1.           #value of one tick (second) unit of time in dollars
    b_carHasPassed = (y<0.)     # only when car passes over 0 
    b_pedHaspassed = (x<=0.)   # check whether the ped has already crossed
    if b_carHasPassed or b_pedHaspassed:
        x_new = x - pedspeed   #cross
        U_prox_given_walk = 0.

    if not b_carHasPassed and not b_pedHaspassed :      
        veh_time_to_inters = y/abs(carspeed)
        ped_time = x/pedspeed    #time it would take ped to cross

        assert_almost_equal(ped_time, 1.0)
        
        U_dontwalk = veh_time_to_inters * U_timedelay   #value of waiting for the vehicle to cross 
        
        if veh_time_to_inters<=ped_time:
            p_crash_given_walk = 1.
        else:
            p_crash_given_walk = 0.

        #TODO compute U_prox.  In theory, U_prox=\int dt U_ped_prox(x,y)  . We approximate with the single longest arrow.
        #(we assume that the interaction lasts for a infinitely small time)
        #eg. car could be 2km away now.  but it will be closer in the future at time t2. 
        #what is the single WORST proxemic utility we will experience if we cross?
        if(int(y)==0):
            U_prox_given_walk = -float("Inf")
        else:
            U_prox_given_walk = U_ped_prox_fn(y)    #TODO think about the details and assumptions/approximations here
       
        U_walk = p_crash_given_walk*U_ped_crash + U_prox_given_walk
    
        if U_walk>U_dontwalk:    #make our action selection decision
            x_new = x - pedspeed
        else:
            x_new = x

    return x_new  


def simulate(U_ped_crash, U_ped_prox, y_init, carspeed):
    T = 2     # only runs for one time: walk or stop
    x_init = 1.0  #start meters from collision point for pedestrian
    pedspeed = 1.0 #ped speed 1 meter per second

    xs=[x_init]
    ys=[y_init]

    x=x_init
    y=y_init    
    for t in range(0, T):
        x = movePed(x,y, pedspeed, carspeed, U_ped_crash, U_ped_prox)
        y = moveCar(y,carspeed)

        xs.append(x)
        ys.append(y)
    return (xs,ys)

def simulateMany(U_ped_crash, U_ped_prox, carspeed):
    epochs = 50   #how many simulations to run for one set of parameters

    alldata = []
    for epoch in range(0, epochs):
        y_init = random.randint(0, Y_MAX)   #meters distance from collision point for car start location, random selection
        (xs, ys) = simulate(U_ped_crash, U_ped_prox, y_init, carspeed) 
        
        simdata = (y_init, carspeed, xs, ys) 
        alldata.append(simdata)
    
    for simdata in alldata:
        print(simdata)
    
    return (alldata)
    
    
### This function converts interaction into a game and returns a list of games as (y, x, a_y, a_x)
def createGames(simData, carSpeed):
    allgames = []
    for sim in simData:
        games = []
        x_prev = 2.
        for i in range(0, len(sim[2])):
            y = sim[3][i]
            x = sim[2][i]
            if(y>=0. and x>=0.):
                a_y = carSpeed
                a_x = x_prev - x
                turn = (y, x, a_y, a_x)
                x_prev = x
                games.append(turn)
        allgames.append(games)
    print("Games:")
    print(allgames)
    return allgames		   
       
   
### This function computes the histogram model given the simulatio data        
def histogramData(simData):
   #### this block computes the frequencies for walking and stopping per y 
    freq = []   # contains frequencies for each value y
    for y in range(0, Y_MAX+1):
        fw = 0
        fs = 0
        for sim in simData:
            if(int(sim[0])== int(y) and int(sim[2][1])==0):  # check value of y and that x walks
                fw +=1.
            elif(int(sim[0])== int(y) and int(sim[2][1])==1):   # check value of y and that x stops
                fs +=1.
        freq.append((y, fw, fs))
    #print("Freq:\n" + str(freq))

    #### this block computes the probabilities given the frequencies from above
    pedWalk = tuple([x[1] for x in freq])
    pedStop = tuple([x[2] for x in freq])     
    pHist = []
    for y in range(0,Y_MAX+1):
        if(int(pedWalk[y] + pedStop[y]) == 0):
            pw = 0.
            ps = 0.
        else:
            pw = (pedWalk[y] + 0.)/(pedWalk[y] + 0.  + pedStop[y] + 0.)    # frequency of walk in histogram for a given y
            ps = 1. - pw
        pHist.append((pw, ps))    # probability
        
    #### this block creates the bar chart  (uncomment below to show the plots)
    fig, ax = plt.subplots()
    index = np.array((range(0, Y_MAX+1, 1)))
    bar_width = 0.35
    opacity = 0.8       
    rects1 = plt.bar(index, pedWalk, bar_width, alpha=opacity,color='b',label='Walk')
    rects2 = plt.bar(index, pedStop, bar_width, alpha=opacity, color='g', label='Stop')
    plt.xlabel('Car distance y')
    plt.ylabel('Pedestrian\'s choice')
    plt.title('Pedestrian crossing choice by car distance')
    xlabels = np.array((range(0, Y_MAX+1, 1)))
    xlabels = [str(xlabels[i]) for i in xlabels]
    plt.xticks(index, xlabels)
    plt.legend()
    plt.show()
    return freq, pHist	        
    

### This function assumes that we use a hyperbolic model with our ground truth utility function {f(x)=1/x} 
def hyperbolicModel(p):
    U_prox_fn = (lambda dist: 1./dist)
    y = np.arange(p+1)
    up = []
    for i in range(0, len(y)):
        if(i == 0):
            up.append((i, 1))
        else:
            up.append((i, U_prox_fn(i)/(p+1)))   
    return up
    
### This function plots the histogram of the proxemic utility function    
def plotHist(up, title):
    bar_width = 0.35
    opacity = 0.8       
    rects1 = plt.bar([x[0] for x in up], [x[1] for x in up], bar_width, alpha=opacity, color='b')
    plt.xlabel('Car distance y')
    plt.ylabel('Pedestrian Proxemic utility')
    plt.title(title + ' Pedestrian Proxemic Uility by Car Distance')
    xlabels = np.array((range(0, len([x[0] for x in up])+1, 1)))
    xlabels = [str(xlabels[i]) for i in xlabels]
    plt.xticks([x[0] for x in up], xlabels)
    plt.show()
 

### This function finds the best parameters that fit the Hyperbolic model given the data
def findBestParametersHyperbolicModel(dataGen, carSpeed) :
    games = createGames(dataGen, carSpeed)
    results = []		#likelihood values for all params as a list of tuples (U_prox, likelihood)
    lik_all_games = 0.	
    lik_X = 0
    
    for param in range(0, 100, 1):
        print("\nParam: " + str(param))
        u_prox = hyperbolicModel(param)
        up = []
        lik_all_games = 0.
        for game in games:
            lik_game = 0.
            for turn in games:
                (y,x, a_y,a_x) = turn[0]	   
                if y<=param:
                    lik_X = u_prox[y][1]  + 0.00000000001 # some noise
                else:
                    lik_X = 0.00000000001     # noise
                lik_turn = np.log(lik_X)  
                lik_game+= lik_turn
            lik_all_games += lik_game
        print("lik all games: " + str(lik_all_games))
        up.append((param, u_prox))
        results.append((up, lik_all_games))
    return results


### This function 
### histogramModel(param_a, param_b ... param_z, data):
### return likelihood P(data| model, parm_a, param_b, param_z)
def histogramModel(data, carSpeed):
    freq, pHist = histogramData(data)
    pedStop = [x[1] for x in freq]
    sumFreq = sum(pedStop)
     
    games = createGames(dataGen, carSpeed)
    results = []		#likelihood values for all params as a list of tuples (U_prox, likelihood)
    lik_all_games = 0.	
    lik_X = 0
     
    for nBins in range(1, 8):
        print("\nnBins: " + str(nBins))
        binHeights = []
        for j in range(0, nBins, 1):
            print("j: " + str(j))
            print("j start: " + str(int(round(j*(Y_MAX+1)/nBins))))
            print("j end: " + str(int(round((j+1)*(Y_MAX+1)/nBins))))
            print("pedStop j: " + str(pedStop[int(round(j*(Y_MAX+1)/nBins)):int(round((j+1)*(Y_MAX+1)/nBins))]))
            newHeights = sum(pedStop[int(round(j*(Y_MAX+1)/nBins)):int((j+1)*(Y_MAX+1)/nBins)])
            newHeights = newHeights/sumFreq
            print("NewHeights:" + str(newHeights))
            binHeights.append((int(round((j)*(Y_MAX+1)/nBins)), 1 - newHeights))
        print("binHeights:" + str(binHeights))
        up = []              
        lik_all_games = 0.
        for game in games:
            lik_game = 0.
            for turn in games:
                (y,x, a_y,a_x) = turn[0]	 
                #print("Y: " + str(y))  
                if y < nBins:
                    lik_X = 1 - binHeights[y][1]  + 0.00000000001     # noise
                    #print("Y: " + str(y) + " Lik_X: " + str(lik_X))
                #elif y == nBins:
                #    lik_X = 1 - binHeights[y-1][1]  + 0.00000000001     # noise
                else:
                    lik_X = 0.00000000001     # noise
                lik_turn = np.log(lik_X)  
                lik_game+= lik_turn
            lik_all_games += lik_game
        print("lik all games: " + str(lik_all_games))
        up.append((nBins, binHeights))
        results.append((up, lik_all_games))
    return results  
     

  
### Plot the utility histogram with the best parameters	
def plot(res, title):
    best_param = max(res, key=operator.itemgetter(1))[0]
    print("Best param: " + str(best_param))
    rect = plt.bar([x[0] for x in best_param[0][1]], [x[1] for x in best_param[0][1]], 0.35, alpha=0.8, color='b')
    plt.grid(True)
    plt.xlabel('Y')
    plt.ylabel('Likelihood')
    plt.title("Best " + title + " Proxemic Utility Graph")
    #plt.show()	

### Cubic Model
def cubicModel(dataGen, carSpeed):
    
    games = createGames(dataGen, carSpeed)
    results = []		#likelihood values for all params as a list of tuples (U_prox, likelihood)
    lik_all_games = 0.	
    lik_X = 0
    
    cubic = (lambda dist, a, b, c, d: np.poly1d([a, b, c, d])(dist))
    
    for a in range(-10, 0, 1):
        for b in range(-10, 1, 1):
            for c in range(-100, 0, 50):
                for d in range(-100, 110, 50):                    
                    normalizer = 0
                    for i in range(0,Y_MAX+1):
                        normalizer += cubic(i, a, b, c, d) 
                    normalizer += 0.00000000001            # some noise   
                    print("Normalizer: " + str(normalizer))        
                    up = []
                    lik_all_games = 0.    
                    for game in games:
                        lik_game = 0.
                        for turn in games:
                            (y,x, a_y,a_x) = turn[0]	   
                            y_cubic =  cubic(y, a, b, c, d) #/normalizer # some noise
                            #print("ycubic: " +str(y_cubic))
                            #print("Lik_X: " + str(lik_X))
                            if y_cubic < 0.:
                                lik_X = 0.00000000001     # some noise
                            else:
                                lik_X = y_cubic
                            lik_turn = np.log(lik_X)
                            #print("\nLik_turn: " + str(lik_turn))
                            lik_game += lik_turn
                            #print("\nLik_game: " + str(lik_game))
                        lik_all_games += lik_game
                    print("lik all games: " + str(lik_all_games))
                    param = (a, b, c, d)
                    print("\nParam: " + str(param))
                    up.append((param, normalizer))
                    results.append((up, lik_all_games))
    return results

 
 
def plotCubicResult(res):
    best_param = max(res, key=operator.itemgetter(1))[0]
    print("Best param: " + str(best_param))
    poly = np.poly1d(np.array(best_param[0][0]))
    #print("Best Cubic Function: " + str(poly))
    normalizer = best_param[0][1]
    print("Normalizer: " + str(normalizer))
    y = np.arange(0, Y_MAX+1)
    print("Y: " + str(y))
    plt.plot(y, poly(y)/normalizer)
    plt.grid(True)
    plt.xlabel('Y')
    plt.ylabel('U_prox')
    plt.title("Best Cubic Proxemic Utility Graph")
    plt.show()	



def gaussianModel(dataGen, carSpeed):

    games = createGames(dataGen, carSpeed)
    results = []		#likelihood values for all params as a list of tuples (U_prox, likelihood)
    lik_all_games = 0.	
    lik_X = 0
    
    for mu in range(0, Y_MAX+1, 1):
        mu = mu + 0.00000000001    # some noise
        for sigma in range(0, 20, 1):
            sigma += 0.5
            up = []
            lik_all_games = 0.
            for game in games:
                 lik_game = 0.
                 for turn in games:
                     (y,x, a_y,a_x) = turn[0]	 
                     var = float(sigma)**2
                     denom = (2*math.pi*var)**.5
                     num = math.exp(-(float(y)-float(mu))**2/(2*var))  
                     lik_X =  num/denom 
                     print("Lik_X: " + str(lik_X))
                     lik_turn = np.log(lik_X)
                     lik_game+= lik_turn
                     print("\nLik_game: " + str(lik_game))
                     param = (mu,sigma)
                     print("\nParam: " + str(param))
                 print("\nLik_game: " + str(lik_game))
                 up.append(param)
                 lik_all_games += lik_game
            print("lik all games: " + str(lik_all_games))
            results.append((up, lik_all_games))
    return results  


def plotGaussian(res):
    best_param = max(res, key=operator.itemgetter(1))[0]
    print("Best param: " + str(best_param))
    y = np.linspace(0, Y_MAX+1, 100)
    gaussian = scipy.stats.norm.pdf(y, best_param[0][0], best_param[0][1])
    plt.plot(y, gaussian, color='coral')
    plt.grid(True)
    plt.xlabel('Y')
    plt.ylabel('U_prox')
    plt.title("Best Gaussian Proxemic Utility Graph")
    #plt.show()
 



if __name__=="__main__":
    ### parameters
    carSpeed = -2  #car speed in meters per second 
    U_ped_crash = -100.
    U_ped_prox_fn = (lambda dist: 1./dist)
    
    ### data generation
    dataGen = simulateMany(U_ped_crash, U_ped_prox_fn, carSpeed)
    size = max(dataGen, key=operator.itemgetter(0))[0]
    upTruth = hyperbolicModel(size)
    plotHist(upTruth, 'Ground Truth')
    
    ### histogram of data
    #histogramData(dataGen)	
    

    ### Hyperbolic Function Parameter fitting
    resultsHyperbolic = findBestParametersHyperbolicModel(dataGen, carSpeed) 
    
    ### Histogram Model
    #results = histogramModel(dataGen, carSpeed) 
    #plot(results, "Histogram Model")
    
    ## Cubic Function Parameter fitting
    #results = cubicModel(dataGen, carSpeed)
    #plotCubicResult(results)
    
    ### Gaussian Model
    resultsGaussian = gaussianModel(dataGen, carSpeed)
    plot(resultsHyperbolic, "Hyperbolic")
    plotGaussian(resultsGaussian)
    
    
    plt.show()
